package com.javagda22.zadania.zad_rower;

public enum TypRoweru {
    ROWER(1),
    TANDEM(2);

    // + pole w enum
    private int iloscMiejsc;

    // + konstruktor
    TypRoweru(int iloscMiejsc) {
        this.iloscMiejsc = iloscMiejsc;
    }

    public int pobierzIloscMiejsc(){
        return iloscMiejsc;
    }
}

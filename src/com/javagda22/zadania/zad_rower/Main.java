package com.javagda22.zadania.zad_rower;

public class Main {
    public static void main(String[] args) {
        Rower[] rowers = new Rower[3];
        // stworzyć 3 rowery i umieścić je w tabeli
        rowers[0] = new Rower(4, TypRoweru.TANDEM, "Bajk");
        rowers[1] = new Rower(6, TypRoweru.ROWER, "Super hiper mega kros");
        rowers[2] = new Rower(10, TypRoweru.ROWER, "Rower górski");

        // następnie wypisać wszystkie rowery w pętli FOR
        for (int i = 0; i < rowers.length; i++) {
            System.out.println(rowers[i]);

            // *dodatkowo wywołać zmienną z rowerów:
            rowers[i].wypiszInformacjeORowerze();
        }
    }
}

package com.javagda22.zadania.zad_formularz_obywatela;

public class Obywatel {
    private String imie;
    private String nazwisko;
    private Plec plec; //

    // BAAARDZO ŹLE!
//    private enum plec;
    private String pesel;

    public Obywatel(String imie, String nazwisko, Plec plec, String pesel) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return "Obywatel{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", plec=" + plec +
                ", pesel='" + pesel + '\'' +
                '}';
    }
}

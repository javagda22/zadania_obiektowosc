package com.javagda22.zadania.zad_formularz_obywatela;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imie:");
        String imie = scanner.next();

        System.out.println("Podaj nazwisko:");
        String nazwisko = scanner.next();

        System.out.println("Podaj pesel:");
        String pesel = scanner.next();

        System.out.println("Podaj plec:");
        String plec = scanner.next();

        // Zamiana tekstu String na typ Plec (enum)
        Plec plecObywtaela = Plec.valueOf(plec.toUpperCase()); //

        // Tworzymy obywatela:
        Obywatel obywatel = new Obywatel(imie, nazwisko, plecObywtaela, pesel);
        System.out.println(obywatel); // wypisanie na konsolę wyniku
        // TADAAAAA!
    }
}

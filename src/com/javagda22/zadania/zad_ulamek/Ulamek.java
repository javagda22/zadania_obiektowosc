package com.javagda22.zadania.zad_ulamek;

public class Ulamek {
    private int licznik;
    private int mianownik;

    public Ulamek(int licznik, int mianownik) {
        this.licznik = licznik;
        this.mianownik = mianownik;
    }

    public void dodaj(Ulamek ulamek) {
        if (ulamek.mianownik == this.mianownik) {
            // jeśli mianowniki są takie same,
            // to wystarczy dodać do siebie liczniki
            this.licznik += ulamek.licznik;
        } else {
            // przykład  11/15 + 20/17
            // licznik1 = (11*17)
            // licznik2 = (20*15)
            // mianownik = (15*17)
            int licznik1 = this.mianownik * ulamek.licznik;
            int licznik2 = ulamek.mianownik * this.licznik;

            this.mianownik *= ulamek.mianownik;
            this.licznik = licznik1 + licznik2;
        }
    }


    public void odejmij(Ulamek ulamek) {
        if (ulamek.mianownik == this.mianownik) {
            // jeśli mianowniki są takie same,
            // to wystarczy odjąć od siebie liczniki
            this.licznik -= ulamek.licznik;
        } else {
            // przykład  11/15 + 20/17
            // licznik1 = (11*17)
            // licznik2 = (20*15)
            // mianownik = (15*17)
            int licznik1 = this.mianownik * ulamek.licznik;
            int licznik2 = ulamek.mianownik * this.licznik;

            this.mianownik *= ulamek.mianownik;
            this.licznik = licznik1 - licznik2;
        }
    }

    public void mnożenie(Ulamek ulamek) {
        this.licznik *= ulamek.licznik;
        this.mianownik *= ulamek.mianownik;
    }

    public void dzielenie(Ulamek ulamek) {
        this.licznik *= ulamek.mianownik;
        this.mianownik *= ulamek.licznik;
    }

    public int getLicznik() {
        return licznik;
    }

    public void setLicznik(int licznik) {
        this.licznik = licznik;
    }

    public int getMianownik() {
        return mianownik;
    }

    public void setMianownik(int mianownik) {
        this.mianownik = mianownik;
    }

    @Override
    public String toString() {
        return "Ulamek{" +
                "licznik=" + licznik +
                ", mianownik=" + mianownik +
                '}';
    }
}

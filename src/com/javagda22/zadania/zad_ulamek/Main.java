package com.javagda22.zadania.zad_ulamek;

public class Main {
    public static void main(String[] args) {
        Ulamek ulamek1 = new Ulamek(17, 15);
        Ulamek ulamek2 = new Ulamek(20, 15);

        System.out.println(ulamek1);

        ulamek1.dodaj(ulamek2);

        System.out.println(ulamek1);
    }
}

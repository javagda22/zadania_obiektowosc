package com.javagda22.zadania.zad_string;

import java.util.Scanner;

public class Main3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz linię tekstu:");

        String tekst = scanner.nextLine();

        System.out.println("Wpisz słowo:");
        String searched = scanner.next();

        if(tekst.contains(searched)){
            System.out.println("Tekst zawiera szukane słowo.");
        }else{
            System.out.println("Tekst nie zawiera szukanego słowa!");
        }
    }
}

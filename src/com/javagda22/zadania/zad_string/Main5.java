package com.javagda22.zadania.zad_string;

public class Main5 {
    public static void main(String[] args) {
/*
5. Napisz program który zlicza wystąpienia liter w tekście.
    Posłuż się do tego pętlami for/while. (spróbuj zaimplementować na dwa sposoby).

    sposób 1/wskazówka 1: wykorzystaj kody ascii liter i iteruj tekst tyle razy, ile mamy liter w alfabecie i porównuj litery z kodami ascii
    *sposób 2/wskazówka 2: wykorzystaj do zliczania dodatkową tablicę.
 */
        String tekst = "ala ma kota";

        // tablica z ilościami wystąpień
        // poszczególnych znaków.
        // 25 - ponieważ mamy tyle znaków
        int[] zliczenia = new int[26];
        tekst = tekst.toLowerCase();
        for (int i = 0; i < tekst.length(); i++) {
            char znak = tekst.charAt(i);
            int ascii = znak;
            // szukamy / zliczamy małe litery
            // a - 97
            // z - 122

            // zliczenia['a']
            // zliczenia[97]
            if (ascii >= 97 && ascii <= 122) {
                zliczenia[ascii - 'a']++;
            }
            // wskazówka 1 : tak jak zliczanie cyrfr
            // wskazówka 2 : 97-97 = 0
            // wskazówka 3: tablica znaków r.26
        }

        for (int i = 0; i < zliczenia.length; i++) {
            if (zliczenia[i] > 0) { // <- doszła ta linia
                System.out.println(((char) (i + 'a')) + " -> " + zliczenia[i]);
            }   // <- doszła ta linia
        }
    }
}

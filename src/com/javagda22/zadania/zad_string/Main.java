package com.javagda22.zadania.zad_string;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj linię tekstu:");
        // czytamy jedną linię tekstu
        String text = scanner.nextLine();

        //sprawdź czy w tekście występuje słowo “ania” - contains
        if (text.contains("ania")) {
            System.out.println("Tekst zawiera słowo 'ania'");
        }

        //sprawdź czy tekst zaczyna się od słowa “ania” - startsWith
        if (text.startsWith("ania")) {
            System.out.println("Tekst rozpoczyna się od 'Ania'");
        } else {
            System.out.println("Tekst nie rozpoczyna się od 'Ania'");
        }

        //sprawdź czy tekst równa się słowu “ania” - equals
        if (text.endsWith("ania")) {
            System.out.println("Tekst kończy się na 'ania'");
        } else {
            System.out.println("Tekst nie kończy się na 'ania'");
        }

        //sprawdź czy tekst równa się słowu “ania” - equals
//        if (text.equalsIgnoreCase("ania")) {
        if (text.equals("ania")) {
            System.out.println("Tekst kończy się na 'ania'");
        } else {
            System.out.println("Tekst nie kończy się na 'ania'");
        }


        // sprawdź czy tekst (niezależnie od wielkości liter) zawiera słowo “ania” - toLowerCase + contains
        // zamieniamy na małe litery i sprawdzamy
        // czy podany w argumencie tekst występuje
        // w tekście w zmiennej 'text'
        if (text.toLowerCase().contains("ania")) {
            System.out.println("Tekst zawiera slowo 'ania' (toLowerCase)");
        } else {
            System.out.println("Tekst nie zawiera slowo 'ania' (toLowerCase)");
        }

        // pozycja to pozycja na której został odnaleziony
        // tekst "ania"
        int pozycja = text.indexOf("ania");
        if (pozycja >= 0) {
            System.out.println("Tekst występuje na pozycji:" + pozycja);
        } else {
            System.out.println("Tekst nie występuje w zdaniu.");
        }
    }
}

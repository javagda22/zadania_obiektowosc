package com.javagda22.zadania.zad_konto_bankowe;

public class Main {
    public static void main(String[] args) {
        KontoBankowe kontoAndrzeja = new KontoBankowe(123L, 1000);


        KontoBankowe kontoBeaty = new KontoBankowe(555L, 2000);
        // wyświetlam stan konta
        kontoAndrzeja.wyswietlStanKonta();

        // wplacam 50 pieniążków
        kontoAndrzeja.wplacSrodki(50);

        // wyświetlam ponownie stan konta (zmieniony)
        kontoAndrzeja.wyswietlStanKonta();

        int ileRzeczywiscieWyplacilismy = kontoAndrzeja.pobierzSrodki(5000);
        System.out.println("Wyplacilismy: " + ileRzeczywiscieWyplacilismy);

        kontoBeaty.wyswietlStanKonta();
        // zmienna ileRzeczywiscieWyplacilismy zawiera ilosc pieniedzy do przelania
        // na konto Beaty
        kontoBeaty.wplacSrodki(ileRzeczywiscieWyplacilismy);


        // wyświetlamy finalny stan konta Beaty
        kontoBeaty.wyswietlStanKonta();
    }
}

package com.javagda22.zadania.zad_konto_bankowe;

import com.javagda22.zadania.zad_kalkulator.Kalkulator;

public class KontoBankowe {
    private long numerKonta;
    private int stanKonta;

    public long getNumerKonta() {

        return numerKonta;
    }

    public void setNumerKonta(long numerKonta) {
        this.numerKonta = numerKonta;
    }

    public int getStanKonta() {
        return stanKonta;
    }

    public void setStanKonta(int stanKonta) {
        this.stanKonta = stanKonta;
    }

    // konstruktor - czyli funkcja tworząca obiekt
    // zawiera modyfikator dostępu (public)
    // zawiera nazwę konstruktora która jest taka sama jak nazwa klasy (KontoBankowe)
    // zawiera parametry niezbędne do stworzenia obiektu - czyli numer i stan konta.
    public KontoBankowe(long numerKonta, int stanKonta) { // <- parametry metody/konstr
        // this = ten obiekt
        // this.numerKonta = numerKonta - do pola numerKonta z tego obiektu przypisz
        //                  wartość zmiennej numerKonta z parametru ( w nawiasach wyżej)
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    // metoda publiczna - dostępna wszędzie
    // nic nie zwraca (void)
    // nazwa - wyswietlStanKonta
    // () - brak parametrów
    public void wyswietlStanKonta() {
        System.out.println("Stan konta wynosi: " + stanKonta + " pieniążków.");
    }

    // metoda publiczna - dostępna wszędzie
    // nic nie zwraca (void)
    // nazwa - wplacSrodki
    // (int ileWplacicNaKonto) - brak parametrów
    public void wplacSrodki(int ileWplacicNaKonto) {
        // zachowanie metody - dodaję do obecnego stanu konta 'ileWplacicNaKonto'
        this.stanKonta = this.stanKonta + ileWplacicNaKonto;
    }

    // metoda publiczna - dostępna wszędzie
    // metoda zwraca int - ilość środków którą udało się wypłacić
    // nazwa - pobierzSrodki
    // (int ileWyplacicZKonta) - parametr
    public int pobierzSrodki(int ileWypłacicZKonta) {
        if (this.stanKonta >= ileWypłacicZKonta) { // starczy nam pieniędzy
            // odejmujemy od stanu konta tyle ile wypłacamy.
            this.stanKonta = this.stanKonta - ileWypłacicZKonta;

            // wartość zwracana z funkcji
            return ileWypłacicZKonta;
            // zwracamy dokładnie tyle ile udało się wypłacić
        } else {
            System.out.println("Nie masz tyle pieniązków.");
            // zapisujemy sobie ile mamy pieniedzy (tyle wypłacamy z konta)
            int ileMamPieniedzy = this.stanKonta;
            // zerujemy stan konta
            this.stanKonta = 0; // nie starczy nam pieniędzy, więc zwracamy tyle ile mamy

            return ileMamPieniedzy; // zwracamy resztę pieniędzy z konta.
        }
    }
}

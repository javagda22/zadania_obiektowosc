package com.javagda22.zadania.zad_kalkulator;

public class Main {
    public static void main(String[] args) {
        // tworzę instancję/obiekt Kalkulator
        Kalkulator instancja = new Kalkulator();


        // wynik działania przypisuję do zmiennej
        double wynik = Kalkulator.dodajDwieLiczby(5.0, 10.1);


        // wynik działania (zmienną) wypisuję na ekran
        System.out.println(wynik);
    }
}

package com.javagda22.zadania.zad_kalkulator;

/**
 * Napisz program w którym jest jedna klasa 'Kalkulator'.
 * Klasa reprezentuje Kalkulator. Klasa powinna posiadać metodę:
 * -   dodajDwieLiczby - która przyjmuje dwa parametry i zwraca wynik dodawania
 * -   odejmijDwieLiczby - która przyjmuje dwa parametry i zwraca wynik odejmowania
 * -   pomnóżDwieLiczby - która przyjmuje dwa parametry i zwraca wynik mnożenia
 * -   podzielDwieLiczby - która przyjmuje dwa parametry i zwraca wynik dzielenia
 * <p>
 * UWAGA! nie twórz pól klasy które reprezentują liczby działań. Podstawowym błędem
 * popełnianym w tym zadaniu jest dodanie pól do klasy.
 * Liczby na których masz wykonać działania są przekazywane jako
 * argumenty/parametry metody!!!
 * UWAGA! wynik jest zwracany z metody. W main'ie wykonaj działania na kalkulatorze,
 * a następnie wartość zwróconą przypisz do zmiennej i wyświetl na konsoli
 * <p>
 * Wszystkie metody zwracają wartości. Stwórz maina, a w nim jedną
 * instancję klasy Calculator, a następnie przetestuj działanie wszystkich metod.
 */
public class Kalkulator {

    public static double dodajDwieLiczby(double liczba1, double liczba2) {
        return liczba1 + liczba2;
    }

    public static double odejmijDwieLiczby(double liczba1, double liczba2) {
        return liczba1 - liczba2;
    }

    public static double pomnóżDwieLiczby(double liczba1, double liczba2) {
        return liczba1 * liczba2;
    }

    public static double podzielDwieLiczby(double liczba1, double liczba2) {
        return liczba1 / liczba2;
    }

}

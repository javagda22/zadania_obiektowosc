package com.javagda22.zadania.zad_superbohater;

import java.util.Objects;

public class SuperBohater {
    private String nazwa;
    private String supermoc;

    public SuperBohater(String nazwa, String supermoc) {
        this.nazwa = nazwa;
        this.supermoc = supermoc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SuperBohater that = (SuperBohater) o;
        return Objects.equals(nazwa, that.nazwa) &&
                Objects.equals(supermoc, that.supermoc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazwa, supermoc);
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getSupermoc() {
        return supermoc;
    }

    public void setSupermoc(String supermoc) {
        this.supermoc = supermoc;
    }

    @Override
    public String toString() {
        return "SuperBohater{" +
                "nazwa='" + nazwa + '\'' +
                ", supermoc='" + supermoc + '\'' +
                '}';
    }
}

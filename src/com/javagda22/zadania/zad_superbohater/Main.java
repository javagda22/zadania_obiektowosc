package com.javagda22.zadania.zad_superbohater;

public class Main {
    public static void main(String[] args) {
        SuperBohater sb1 = new SuperBohater("superman", "latanie");
        SuperBohater sb2 = new SuperBohater("hulk", "otyłość");
        SuperBohater sb3 = new SuperBohater("ironman", "pancerność");

        System.out.println("sb1==sb2 : " + (sb1 == sb2));
        System.out.println("sb1.equals(sb2) : " + (sb1.equals(sb2)));

        SuperBohater sb1_copy = sb1;
        System.out.println("sb1==sb1_copy : " + (sb1 == sb1_copy));
        System.out.println("sb1.equals(sb1_copy) : " + (sb1.equals(sb1_copy)));

        SuperBohater sb4 = new SuperBohater("superman", "latanie");
        System.out.println("sb1==sb4 : " + (sb1 == sb4));
        System.out.println("sb1.equals(sb4) : " + (sb1.equals(sb4)));

        ////////////////////////////////////////////////////////////
        System.out.println();
        String tekst1 = "ala"; // pool
        String tekst2 = new String("ala"); // referencja

        System.out.println(tekst1.equals(tekst2)); // poprawnie
        System.out.println(tekst1 == tekst2);   // źle
    }
}

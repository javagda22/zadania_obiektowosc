package com.javagda22.zadania.enum_przyklad;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // w enum nie używamy new!
//        Plec mojaPlec = Plec.MEZCZYZNA;

//        System.out.println(mojaPlec);

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Podaj płeć:");
//        String slowo = scanner.next();
//
//        Plec wartoscPlec = Plec.valueOf(slowo.toUpperCase());
//        System.out.println(wartoscPlec);
        Bilet bilet = Bilet.NORMALNY_CALODOBOWY;
        bilet.wyswietlDaneOBilecie();
        System.out.println(bilet.getCena());
        System.out.println(bilet.getIloscMinut());
    }
}

package com.javagda22.zadania.enum_przyklad;

public enum Bilet {
    ULGOWY_GODZINNY(1.60, 60),    // wywołanie konstruktora bilet
    NORMALNY_GODZINNY(3.20, 60),
    ULGOWY_CALODOBOWY(7.00, 24 * 60),
    NORMALNY_CALODOBOWY(14.00, 24 * 60),
    BRAK_BILETU(140, 1);

    private double cena;
    private int iloscMinut;

    Bilet(double cena, int minut) {
        this.cena = cena;
        this.iloscMinut = minut;
    }

    // do zadnia 8
    public void wyswietlDaneOBilecie() {
//        if (this == Bilet.ULGOWY) {
//            System.out.println("Bilet ulogwy 1-godzinny.");
//        }// else if... // itd.

        switch (this) {
            case BRAK_BILETU:
                System.out.println("Brak biletu");
                break;
            case NORMALNY_CALODOBOWY:
                System.out.println("Bilet calodobowy normalny");
                break;
            case ULGOWY_GODZINNY:
                System.out.println("Bilet ulgowy 1-godzinny.");
                break;
            case NORMALNY_GODZINNY:
                System.out.println("Bilet normalny 1-godzinny.");
                break;
            case ULGOWY_CALODOBOWY:
                System.out.println("Bilet ulgowy calodobowy.");
                break;
        }
    }

    public double getCena() {
        return cena;
    }

    public int getIloscMinut() {
        return iloscMinut;
    }
}
